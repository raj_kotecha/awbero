import ui
import re
import appex
import webbrowser
import objc_util
import codecs
import clipboard

output = ''
def do_extract(sender):
	info = v['txtData'].text
	output = info.replace("awb","safari-http")
	
	if output == '' or output == info:
		output = info.replace("awbs","safari-https")
		
	if output != '':
		v['webview1'].load_html('<a href="' + output + '">Link</a>')
		clipboard.set(output)
		bytes1 = bytearray(output, "utf-8")
		webbrowser.open(output)
		
def write(text):
	file = open("url", "wb+")
	file.write(text.encode("utf-8", "ignore"))
	file.close()
	
def read():
	with codecs.open('url', 'r', 'utf-8') as file_in:
		url = file_in.read()
		return url

v = ui.load_view()
if (appex.get_text() is not None):
	v['txtData'].text = appex.get_text()
v.present('sheet')
v.wait_modal()
#v.close()

#global output
